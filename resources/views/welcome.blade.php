<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">

    <title>Translation App</title>
</head>

<body>

    <div class="container">
        <h2 class="mt-1 text-center">Translation App</h2>
        <form action="/translate" method="POST">
            @csrf
            <div class="d-flex flex-row">
                <div class="mb-3 col-md-5">
                    <label for="InputText" class="form-label">Text</label>
                    <input type="text" name="text" class="form-control" id="InputText">
                </div>
                <div class="mb-3 col-md-2 mx-2">
                    <label for="SelectLang" class="form-label">Select Language</label>
                    <select class="form-select" aria-label="Default select example" id="SelectLang" name="source">
                        <option selected >Select Language</option>
                        <option value="1">English</option>
                        <option value="2">France</option>
                    </select>
                </div>
                <div class="mb-3 col-md-5">
                    <label for="Output" class="form-label">Output</label>
                    <input type="text"  name="output" class="form-control" id="Output">
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Translate</button>
        </form>
    </div>


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-kQtW33rZJAHjgefvhyyzcGF3C5TFyBQBA13V1RKPf4uH+bwyzQxZ6CmMZHmNBEfJ" crossorigin="anonymous">
    </script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.min.js" integrity="sha384-PsUw7Xwds7x08Ew3exXhqzbhuEYmA2xnwc8BuD6SEr+UmEHlX8/MCltYEodzWA4u" crossorigin="anonymous"></script>
    -->
</body>

</html>
