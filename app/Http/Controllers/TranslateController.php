<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Stichoza\GoogleTranslate\GoogleTranslate;

class TranslateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        if($request->source == 1)
            $output = GoogleTranslate::trans($request->text,'en');
        else{
            $output = GoogleTranslate::trans($request->text,'fr');
        }
        return view('hasil', compact('output'));
    }
}
